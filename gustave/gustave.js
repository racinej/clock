setInterval(setGustaveClock, 1000)

const hourHand = document.querySelector('[data-hour-hand]')

function setGustaveClock() {
  const currentDate = new Date()
  setRotationh(hourHand, currentDate.getHours(), currentDate.getMinutes())
}

function setRotationh(element, hour, min) {
  if (hour <= 24)
     if (hour >= 12)
        //                                       heure pile                affectation des min
        element.style.setProperty('--rotation', (0.0+15.0*(hour - 12.0)) + (min * (15.0/60.0)))
  else
        element.style.setProperty('--rotation', (180.0+15.0*(hour - 0.0)) + (min * (15.0/60.0)))
}

setGustaveClock()
