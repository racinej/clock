# Wall clock

This project just shows the current time in different form.

## How to get it

```sh
git clone https://gitlab.com/racinej/clock.git
```

## How to run it

```sh
firefox clock.html
```
