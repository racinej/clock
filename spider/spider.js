/* repeat each 1000 ms so each second */

setInterval(setSpiderClock, 1000)


function setSpiderClock() {
  const currentDate = new Date()

  currentHours = currentDate.getHours();
  currentHours = ("0" + currentHours).slice(-2);

  var digits_h = (""+currentHours).split("");
  setnumhex("h10", digits_h[0])
  setnumhex("h1", digits_h[1])

  currentMinutes = currentDate.getMinutes();
  currentMinutes = ("0" + currentMinutes).slice(-2);
  
  var digits_m = (""+currentMinutes).split("");
  setnumhex("m10", digits_m[0])
  setnumhex("m1", digits_m[1])
}

function setnumhex(element, val) {

  var test = "ok";

  // show all
  document.getElementsByClassName("l0"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l1"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l2"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l3"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l4"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l5"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l6"+element)[0].style.visibility = 'visible'; 
  document.getElementsByClassName("l7"+element)[0].style.visibility = 'visible'; 

  // hidden depends of number
  switch (Number(val))
  {
    case (0):
      document.getElementsByClassName("l6"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l7"+element)[0].style.visibility = 'hidden';
      break;
    case (1):
      document.getElementsByClassName("l2"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l3"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l4"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l5"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l6"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l7"+element)[0].style.visibility = 'hidden';
      break;
    case (2):
      document.getElementsByClassName("l0"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l3"+element)[0].style.visibility = 'hidden';
      break;
    case (3):
      document.getElementsByClassName("l0"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l1"+element)[0].style.visibility = 'hidden';
      break;
    case (4):
      document.getElementsByClassName("l1"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l4"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l5"+element)[0].style.visibility = 'hidden';
      break;
    case (5):
      document.getElementsByClassName("l1"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l2"+element)[0].style.visibility = 'hidden';
      break;
    case (6):
      document.getElementsByClassName("l2"+element)[0].style.visibility = 'hidden';
      break;
    case (7):
      document.getElementsByClassName("l0"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l1"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l5"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l6"+element)[0].style.visibility = 'hidden';
      document.getElementsByClassName("l7"+element)[0].style.visibility = 'hidden';
      break;
    case (8):
      test = "oki";
      break;
    case (9):
      document.getElementsByClassName("l1"+element)[0].style.visibility = 'hidden';
      break;
  }
}

setSpiderClock()

